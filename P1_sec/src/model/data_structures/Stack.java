package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class Stack<T> implements IStack<T>
{
	/**Primer nodo de la pila 
	 */
	private Node<T> primero;
	/**
	 * Contador de elementos.
	 */
	private int N = 0;
	/**
	 * Contructor de la clase donde se inicaliza el arreglo dinamico.
	 */
	public Stack()
	{
		primero=null;
    	N=0;
	}
	
	@Override
	public  Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return new IteratorProyecto<>(primero);
	}

	@Override
	public boolean isEmpty() 
	{
		return N == 0;
	}

	@Override
	public int size() 
	{	
		return N;
	}

	@Override
	public void push(T t) 
	{
		 Node<T> viejoPrimero = primero;      
		 primero = new Node<T>(t,viejoPrimero);      
		 N++; 
	}

	@Override
	public T pop() 
	{
		T x = primero.getElemento();
		primero=primero.getSiguiente();
		N--;
		return x;
		
	}
	

}
