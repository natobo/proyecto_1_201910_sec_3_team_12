package model.vo;

public class VOViolationCode implements Comparable<VOViolationCode>
{

	private String ViolationCode;
	
	private int FineAMT;
	
	public VOViolationCode(String pViolationCode, int pFineAMT){
		ViolationCode = pViolationCode;
		FineAMT = pFineAMT;
	}
	
	
	public String getViolationCode() {
		return ViolationCode;
	}
	
	public double getAvgFineAmt() {
		return FineAMT;
	}

	@Override
	public int compareTo(VOViolationCode o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
