package model.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{

	/**
	 * objectId del auto
	 */
	private String objectId;
	/**
	 * violationDescription del auto
	 */
	private String violationDescription;
	/**
	 * totalPaid del auto
	 */
	private String totalPaid;
	/**
	 * accidentIndicator del auto.
	 */
	private String accidentIndicator;
	/**
	 * ticketIssueDate del auto.
	 */
	private LocalDateTime ticketIssueDate;
	/**
	 * violationCode del auto.
	 */
	private String violationCode;
	/**
	 * FineAMT de la infraccion.
	 */
	private String FineAMT;
	/**
	 * Direccion donde ocurrio la infraccion.
	 */
	private String Streetsegid ; 
	/**
	 * Address_Id de la infraccion.
	 */
	private String Address_Id;  
	/**
	 * Multa 1 de la infraccion
	 */
	private String penalty1;
	/**
	 * Multa 2 de la infraccion
	 */
	private String penalty2;
	/**
	 * Locacion de la infraccion
	 */
	private String location;
	/**
	 * constructor
	 */
	public VOMovingViolations(String pObjectId,String pViolationDescription,String pLocation,String pTotalPaid,String pAccidentIndicator,LocalDateTime pTicketIssueDate,String pViolationCode,String pFineAMT,String pStreet, String pAddress_Id,String pPenalty1,String pPenalty2)
	{ 
		objectId=pObjectId;
		violationDescription=pViolationDescription;
		totalPaid=pTotalPaid;
		accidentIndicator=pAccidentIndicator;
		ticketIssueDate=pTicketIssueDate;
		violationCode=pViolationCode;
		FineAMT=pFineAMT;
		Streetsegid=pStreet;
		Address_Id=pAddress_Id;
		penalty1=pPenalty1;
		penalty2=pPenalty2;
		location=pLocation;
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() 
	{
		// TODO Auto-generated method stub
		return Integer.parseInt(objectId);
	}	


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() 
	{
		// TODO Auto-generated method stub
		return location ;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public LocalDateTime getTicketIssueDate() 
	{
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() 
	{
		// TODO Auto-generated method stub
		return Integer.parseInt(totalPaid);
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() 
	{
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() 
	{
		// TODO Auto-generated method stub
		return violationDescription;
	}

	public String getStreetSegId()
	{
		return Streetsegid;
	}

	public String getViolationCode()
	{
		return violationCode;
	}

	public String getFineAMT()
	{
		return FineAMT;
	}


	public String getAddressId() 
	{
		return Address_Id;
	}
	
	public String getPenalty1()
	{
		return penalty1;
	}
	
	public String getPenalty2()
	{
		return penalty2;
	}

	@Override
	public int compareTo(VOMovingViolations o) 
	{
		// TODO Auto-generated method stub
		int  rta=Integer.parseInt(objectId)- o.objectId();
		return rta;
	}

	public int compareToTicketIssueDate(VOMovingViolations o)
	{
		return ticketIssueDate.compareTo(o.getTicketIssueDate());
	}
	
	public int compareToFineAMT(VOMovingViolations o)
	{
		return FineAMT.compareTo(o.getFineAMT());
	}
	
	public int compareToStreetsegid(VOMovingViolations o){
		return Streetsegid.compareTo(o.getStreetSegId());
	}
	
	public int compareToViolationCode(VOMovingViolations o){
		return violationCode.compareTo(o.getViolationCode());
	}
	
	public int compareToViolationDescription(VOMovingViolations o){
		return violationDescription.compareTo(o.getViolationDescription());
	}

	public int compareToTotalPaid(VOMovingViolations o)
	{
		return getTotalPaid() - o.getTotalPaid();
	}
	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "La infraccion: "+objectId+" ocurrio en: "+"AAA"+" el dia: "+ticketIssueDate+" se tiene que pagar: "+totalPaid +"tiene descripcion: "+violationDescription+ ", tiene codigo: "+violationCode+" tiene de adressId: "+Address_Id;
	}
}
