package controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;
import com.opencsv.CSVReader;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.IteratorProyecto;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.util.Sort;
import model.vo.VOMovingViolations;
import model.vo.VOViolationCode;
import view.MovingViolationsManagerView;

public class Controller 
{

	private MovingViolationsManagerView view;

	/**
	 * Lista de meses del programa
	 */
	private String[] listaMes=new String[12];
	/**
	 * Cola donde se carga las infracciones del  cuatrimestre.
	 */
	private Queue<VOMovingViolations> colaCuatrimestreInfracciones;
	
	/**
	 * numero total de infracciones
	 */
	private int numTotalInfrac;
	

	/**
	 * Constructor del controlador
	 */
	public Controller() 
	{
		view = new MovingViolationsManagerView();

		listaMes[0]="January";
		listaMes[1]="February";
		listaMes[2]="March";
		listaMes[3]="April";
		listaMes[4]="May";
		listaMes[5]="June";
		listaMes[6]="July";
		listaMes[7]="August";
		listaMes[8]="September";
		listaMes[9]="October";
		listaMes[10]="November";
		listaMes[11]="December";

		colaCuatrimestreInfracciones=new Queue<VOMovingViolations>();

		numTotalInfrac=0;
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:

				view.printMessage("Ingrese el cuatrimestre (1, 2 o 3)");
				int numeroCuatrimestre = sc.nextInt();
				int rta=-1;
				if(numeroCuatrimestre==1)
					rta=0;
				else if(numeroCuatrimestre==2)
					rta=4;
				else if(numeroCuatrimestre==3)
					rta=8;
				else
				{
					view.printMessage("No se ha igresado correctamente la instruccion");
					break;
				}


				loadMovingViolations(rta);

				break;

			case 1:

				if(colaCuatrimestreInfracciones.isEmpty())
				{
					view.printMessage("La cola de infracciones esta vacia!!!");
					break;
				}

				IQueue<VOMovingViolations> isUnique = verifyObjectIDIsUnique();
				if(isUnique.isEmpty())
				{
					view.printMessage("El objectId es unico en cada infraccion ");
				}
				else
				{
					Iterator<VOMovingViolations> it=isUnique.iterator();
					while (it.hasNext()) 
					{
						VOMovingViolations element=it.next();
						view.printMessage("Los siguientes ObjectId estan repetidos: "+element.objectId());
					}

				}
				break;

			case 2:

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 2018-01-03T13:29:00.000Z)");
				LocalDateTime fechaInicialReq2A = convertirFecha_Hora_LDT(sc.next());

				view.printMessage("Ingrese la fecha con hora final (Ej : 2018-02-03T13:03:00.000Z)");
				LocalDateTime fechaFinalReq2A = convertirFecha_Hora_LDT(sc.next());

				IQueue<VOMovingViolations> resultados2 = getMovingViolationsInRange(fechaInicialReq2A, fechaFinalReq2A);

				view.printMovingViolationsReq2(resultados2);

				break;

			case 3:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode3 = sc.next();

				double [] promedios3 = avgFineAmountByViolationCode(violationCode3);

				view.printMessage("FINEAMT promedio sin accidente: " + promedios3[0] + ", con accidente:" + promedios3[1]);
				break;


			case 4:

				view.printMessage("Ingrese el ADDRESS_ID  (Ej :814983 ) ");
				String addressId4 = sc.next();

				view.printMessage("Ingrese la fecha inicial (Ej : 2018-01-03)");
				LocalDate fechaInicialReq4A = convertirFecha(sc.next());

				view.printMessage("Ingrese la fecha final (Ej : 2018-02-03)");
				LocalDate fechaFinalReq4A = convertirFecha(sc.next());

				IStack<VOMovingViolations> resultados4 = getMovingViolationsAtAddressInRange(addressId4, fechaInicialReq4A, fechaFinalReq4A);

				view.printMovingViolationsReq4(resultados4);

				break;

			case 5:
				view.printMessage("Ingrese el limite inferior de FINEAMT  (Ej: 50)");
				double limiteInf5 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de FINEAMT  (Ej: 50)");
				double limiteSup5 = sc.nextDouble();

				IQueue<VOViolationCode> violationCodes = violationCodesByFineAmt(limiteInf5, limiteSup5);
				view.printViolationCodesReq5(violationCodes);
				break;

			case 6:

				view.printMessage("Ingrese el limite inferior de TOTALPAID (Ej: 200)");
				double limiteInf6 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de TOTALPAID (Ej: 200)");
				double limiteSup6 = sc.nextDouble();

				view.printMessage("Ordenar Ascendentmente: (Ej: true)");
				boolean ascendente6 = sc.nextBoolean();				

				IStack<VOMovingViolations> resultados6 = getMovingViolationsByTotalPaid(limiteInf6, limiteSup6, ascendente6);
				view.printMovingViolationReq6(resultados6);
				break;

			case 7:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial7 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal7 = sc.nextInt();

				IQueue<VOMovingViolations> resultados7 = getMovingViolationsByHour(horaInicial7, horaFinal7);
				view.printMovingViolationsReq7(resultados7);
				break;

			case 8:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode8 = sc.next();

				double [] resultado8 = avgAndStdDevFineAmtOfMovingViolation(violationCode8);

				view.printMessage("FINEAMT promedio: " + resultado8[0] + ", desviacion estandar:" + resultado8[1]);
				break;

			case 9:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial9 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal9 = sc.nextInt();

				int resultado9 = countMovingViolationsInHourRange(horaInicial9, horaFinal9);

				view.printMessage("Numero de infracciones: " + resultado9);
				break;

			case 10:
				String[] datos = datosGrafica();
				view.printMovingViolationsByHourReq10(datos);
				break;

			case 11:
				view.printMessage("Ingrese la fecha inicial (Ej : 2018-01-03)");
				LocalDate fechaInicial11 = convertirFecha(sc.next());

				view.printMessage("Ingrese la fecha final (Ej : 2018-04-03)");
				LocalDate fechaFinal11 = convertirFecha(sc.next());

				double resultados11 = totalDebt(fechaInicial11, fechaFinal11);
				view.printMessage("Deuda total "+ resultados11);
				break;

			case 12:	
				String[] datos2 = datosGrafica2();
				view.printTotalDebtbyMonthReq12(datos2);

				break;

			case 13:	
				fin=true;
				sc.close();
				break;
			}
		}

	}
	/**
	 * 	Carga los csv del cuatrimestre. 
	 */
	public void loadMovingViolations(int numeroCuatrimestre) 
	{		
		colaCuatrimestreInfracciones=new Queue<VOMovingViolations>();
		numTotalInfrac=0;
		
		//Elimina todo rastro de la pila anterior
		Runtime garbage = Runtime.getRuntime();
		garbage.gc();
		
		Queue<VOMovingViolations> colaCargada=new Queue<VOMovingViolations>();
		try 
		{
			int x=numeroCuatrimestre;
			int numTotalInfracciones=0;

			for (int i = 0; i < 4; i++) 
			{
				String xMes=listaMes[x];
				String nombreMes=xMes;
				CSVReader csvReader = new CSVReader(new FileReader("."+File.separator+"data"+File.separator+"Moving_Violations_Issued_in_"+nombreMes+"_2018.csv"));
				String[] fila=null;
				csvReader.readNext();
				int numInfrMes=0;
				while ((fila=csvReader.readNext()) != null) 
				{

					String objectId=fila[0];
					String location= fila[2];
					String FineAMT=fila[8];
					String accidentIndicator=fila[12];
					String totalPaid=fila[9];
					String pStreet=fila[4];
					String pAdress=fila[3];
					String pPenalty1=fila[10];
					String pPenalty2=fila[11];
					String violationCode="";
					LocalDateTime ticketIssueDate=null;
					String violationDescrip="";
					if(xMes.equals("October")|xMes.equals("November")|xMes.equals("December"))
					{
						violationCode=fila[15];
						ticketIssueDate=convertirFecha_Hora_LDT(fila[14]);
						violationDescrip=fila[16];
					}
					else
					{
						violationCode=fila[14];
						ticketIssueDate=convertirFecha_Hora_LDT(fila[13]);
						violationDescrip=fila[15];
					}
					//Crea la infraccion.
					VOMovingViolations infraccion=new VOMovingViolations(objectId, violationDescrip, location, totalPaid, accidentIndicator, ticketIssueDate, violationCode, FineAMT, pStreet, pAdress, pPenalty1, pPenalty2);

					//La agrega a la pila
					numInfrMes++;
					colaCargada.enqueue(infraccion);
				}

				System.out.println("El numero de infracciones del mes "+xMes+" es:"+numInfrMes);

				numTotalInfracciones+=numInfrMes;
				x++;
				csvReader.close();
			} 


			System.out.println("El numero total de infracciones de cuatrimestre es: "+numTotalInfracciones+" infracciones");

			numTotalInfrac+=numTotalInfracciones;

			colaCuatrimestreInfracciones=colaCargada;


		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public IQueue<VOMovingViolations> verifyObjectIDIsUnique() 
	{

		VOMovingViolations[] infracciones=getArregloInfraccionesGeneral();

		//ORDENAMIENTO AQUI:::::
		Sort.ordenarQuickSort(infracciones,"objectId");

		Queue<VOMovingViolations> colaRta=new Queue<VOMovingViolations>();

		int ObjectIdAnt=-1;
		for (VOMovingViolations voMovingViolations : infracciones) 
		{
			if(ObjectIdAnt==-1)
			{
				ObjectIdAnt=voMovingViolations.objectId();
			}
			else
			{
				if(ObjectIdAnt==voMovingViolations.objectId())
				{
					colaRta.enqueue(voMovingViolations);
					ObjectIdAnt=voMovingViolations.objectId();
				}
				ObjectIdAnt=voMovingViolations.objectId();
			}
		}

		return colaRta;
	}

	public IQueue<VOMovingViolations> getMovingViolationsInRange(LocalDateTime fechaInicial,LocalDateTime fechaFinal) 
	{
		// TODO Auto-generated method stub
		Queue<VOMovingViolations> colaRta=new Queue<VOMovingViolations>();

		VOMovingViolations[] infracciones=getArregloInfraccionesGeneral();
		//ORDENAMIENTO AQUI:::::
		Sort.ordenarQuickSort(infracciones,"ticketIssueDate");
		
		boolean Rango=false;
		for (VOMovingViolations voMovingViolations : infracciones) 
		{
			if(voMovingViolations.getTicketIssueDate().isEqual(fechaInicial))
				Rango=true;
			if(Rango)
			{
				colaRta.enqueue(voMovingViolations);
			}
			if(voMovingViolations.getTicketIssueDate().isEqual(fechaFinal))
				Rango=false;
		}
		
		return colaRta;
	}

	public double[] avgFineAmountByViolationCode(String violationCode3) 
	{
		double xNoAccidente=0;
		double totalNoAccidente=0;
		double yAccidente=0;
		double totalAccidente=0;

		VOMovingViolations[] infracciones=getArregloInfraccionesPorCodigo(violationCode3);

		for (VOMovingViolations voMovingViolations : infracciones) 
		{
			if(voMovingViolations!=null&&voMovingViolations.getAccidentIndicator().equals("Yes"))
			{
				yAccidente+=Double.parseDouble(voMovingViolations.getFineAMT());
				totalAccidente++;
			}
			else if(voMovingViolations!=null)
			{
				xNoAccidente=Double.parseDouble(voMovingViolations.getFineAMT());
				totalNoAccidente++;
			}
		}

		double PromedioXnoAcci=xNoAccidente/totalNoAccidente;
		double PromedioAccide=yAccidente/totalAccidente;

		double[] rta=new double [] {PromedioXnoAcci ,PromedioAccide};

		return rta;
	}

	public IStack<VOMovingViolations> getMovingViolationsAtAddressInRange(String addressId,
			LocalDate fechaInicial, LocalDate fechaFinal) 
	{
		Stack<VOMovingViolations> pilaRta=new Stack<VOMovingViolations>();
		// TODO Auto-generated method stub
		VOMovingViolations[] infracciones=getArregloInfraccionesGeneral();

		//Ordena con por un parametro
		Sort.ordenarQuickSort(infracciones,"Streetsegid");
		//Mantiene el orden preestablecido y ordena por fechas.
		//OJO:: como es una pila al momento de realizar el recorrido va a retornar los datos de forma descendente
		Sort.ordenarMergeSort(infracciones, "ticketIssueDate");

		boolean Rango=false;
		for (VOMovingViolations voMovingViolations : infracciones) 
		{
			LocalDate fechaObj=voMovingViolations.getTicketIssueDate().toLocalDate();
			if(fechaObj.isEqual(fechaInicial))
				Rango=true;
			if(Rango)
			{
				if(voMovingViolations.getAddressId().equals(addressId))
					pilaRta.push(voMovingViolations);
			}
			if(fechaObj.isEqual(fechaFinal))
				Rango=false;
		}

		return pilaRta;
	}

	public IQueue<VOViolationCode> violationCodesByFineAmt(double limiteInf5, double limiteSup5) 
	{
		// TODO Auto-generated method stub

		IQueue<VOViolationCode> cola = new Queue<VOViolationCode>();
		VOMovingViolations[] lista = getArregloInfraccionesGeneral();
		Sort.ordenarQuickSort(lista,"FineAMT");
		for(int i = 0; i < lista.length; i++)
		{
			int x = Integer.parseInt(lista[i].getFineAMT());
			if(limiteInf5 <= x && x <= limiteSup5){
				VOViolationCode tal = new VOViolationCode(lista[i].getViolationCode(), x);
				cola.enqueue(tal);
			}
		}
		return cola;
	}


	public IStack<VOMovingViolations> getMovingViolationsByTotalPaid(double limiteInf6, double limiteSup6,boolean ascendente6) 
	{
		// TODO Auto-generated method stub
		IStack<VOMovingViolations> pila = new Stack<VOMovingViolations>();
		VOMovingViolations[] lista = getArregloInfraccionesGeneral();
		Sort.ordenarQuickSort(lista,"TotalPaid");
		for(int i = 0; i < lista.length; i++)
		{
			int x = lista[i].getTotalPaid();
			if(limiteInf6 <= x && x <= limiteSup6){
				pila.push(lista[i]);
			}
		}
		if(ascendente6)
		{
			VOMovingViolations[] sss = getArregloInfraccionesPila(pila);
			Sort.ordenarQuickSort(sss, "ticketIssueDate");
			pila = new Stack<VOMovingViolations>();
			IStack<VOMovingViolations> pq = new Stack<VOMovingViolations>();
			for(int i = 0; i < sss.length; i++){
				pq.push(sss[i]);
			}
			for(int i = 0; i < sss.length; i++){
				pila.push(pq.pop());
			}
		}
		if(!ascendente6)
		{
			VOMovingViolations[] sss = getArregloInfraccionesPila(pila);
			Sort.ordenarQuickSort(sss, "ticketIssueDate");
			pila = new Stack<VOMovingViolations>();
			for(int i = 0; i < sss.length; i++){
				pila.push(sss[i]);
			}
		}
		return pila;
	}

	public IQueue<VOMovingViolations> getMovingViolationsByHour(int horaInicial7, int horaFinal7) 
	{
		// TODO Auto-generated method stub
		IQueue<VOMovingViolations> cola = new Queue<VOMovingViolations>();
		VOMovingViolations[] lista = getArregloInfraccionesGeneral();
		Sort.ordenarQuickSort(lista,"ticketIssueDate");
		for(int i = 0; i < lista.length; i++)
		{
			int x = lista[i].getTicketIssueDate().getHour();
			if(horaInicial7 <= x && x <= horaFinal7){				
				cola.enqueue(lista[i]);
			}
		}
		VOMovingViolations[] sss = getArregloInfraccionesCola(cola);
		Sort.ordenarQuickSort(sss, "violationDescription");
		cola = new Queue<VOMovingViolations>();
		for(int i = 0; i < sss.length; i++){
			cola.enqueue(sss[i]);
		}
		return cola;
	}



	public double[] avgAndStdDevFineAmtOfMovingViolation(String violationCode8) 
	{
		VOMovingViolations[] lista = getArregloInfraccionesGeneral();
		Sort.ordenarQuickSort(lista,"ticketIssueDate");

		double promedio=0;
		double totalViolations=0;

		for (VOMovingViolations voMovingViolations : lista) 
		{
			if(voMovingViolations!=null&&voMovingViolations.getViolationCode().equals(violationCode8))
			{
				Double temp=Double.parseDouble(voMovingViolations.getFineAMT());
				promedio+=temp;
				totalViolations++;
			}
		}

		promedio=promedio/totalViolations;

		double suma=0;
		double temp=0;
		for ( int i = 0; i < lista.length; i++ )
		{
			if(lista[i].getViolationCode().equals(violationCode8))
			{
				temp=Double.parseDouble(lista[i].getFineAMT());
				suma += Math.pow ( temp - promedio, 2 );
			}
		}
		double desviacion= Math.sqrt ( suma / totalViolations );

		return new double [] { promedio, desviacion};
	}



	public int countMovingViolationsInHourRange(int horaInicial9, int horaFinal9) 
	{
		// TODO Auto-generated method stub
		IQueue<VOMovingViolations> cola = new Queue<VOMovingViolations>();
		VOMovingViolations[] lista = getArregloInfraccionesGeneral();
		Sort.ordenarQuickSort(lista,"ticketIssueDate");
		
		for(int i = 0; i < lista.length; i++)
		{
			int x = lista[i].getTicketIssueDate().getHour();
			if(horaInicial9 <= x && x <= horaFinal9)
			{				
				cola.enqueue(lista[i]);
			}
		}
		return cola.size();
	}

	public double totalDebt(LocalDate fechaInicial11, LocalDate fechaFinal11) 
	{
		// TODO Auto-generated method stub
		double totalDeuda=0;
		VOMovingViolations[] lista = getArregloInfraccionesGeneral();
		Sort.ordenarQuickSort(lista,"ticketIssueDate");
		boolean Rango=false;
		for (VOMovingViolations voMovingViolations : lista) 
		{
			if(voMovingViolations.getTicketIssueDate().toLocalDate().isEqual(fechaInicial11))
				Rango=true;
			if(Rango)
			{
			   double FineAMT=voMovingViolations.getFineAMT().equals("")?0:Double.parseDouble(voMovingViolations.getFineAMT());
			   double penalty1=voMovingViolations.getPenalty1().equals("")?0:Double.parseDouble(voMovingViolations.getPenalty1());
			   double penalty2=voMovingViolations.getPenalty2().equals("")?0:Double.parseDouble(voMovingViolations.getPenalty2());
			   int totalPaid=voMovingViolations.getTotalPaid();
			   
			   totalDeuda+=FineAMT+penalty1+penalty2-totalPaid;		
			}
			if(voMovingViolations.getTicketIssueDate().toLocalDate().isEqual(fechaFinal11))
				Rango=false;
		}
		return totalDeuda;
	}

	public String[] datosGrafica()
	{
		String[] XS = new String[24];
		double[] n = new double[24];
		IQueue<VOMovingViolations> cola = new Queue<VOMovingViolations>();
		VOMovingViolations[] lista = getArregloInfraccionesGeneral();
		Sort.ordenarQuickSort(lista,"ticketIssueDate");
		for(int i = 0; i < lista.length; i++)
		{
			int hora = lista[i].getTicketIssueDate().getHour();
			switch(hora){
			case 0:
				n[0] = n[0]+1;
				break;
			case 1:
				n[1] ++;
				break;
			case 2:
				n[2] ++;
				break;
			case 3:
				n[3] ++;
				break;
			case 4:
				n[4] ++;
				break;
			case 5:
				n[5] ++;
				break;
			case 6:
				n[6] ++;
				break;
			case 7:
				n[7] ++;
				break;
			case 8:
				n[8] ++;
				break;
			case 9:
				n[9] ++;
				break;
			case 10:
				n[10] ++;
				break;
			case 11:
				n[11] ++;
				break;
			case 12:
				n[12] ++;
				break;
			case 13:
				n[13] ++;
				break;
			case 14:
				n[14] ++;
				break;
			case 15:
				n[15] ++;
				break;
			case 16:
				n[16] ++;
				break;
			case 17:
				n[17] ++;
				break;
			case 18:
				n[18] ++;
				break;
			case 19:
				n[19] ++;
				break;
			case 20:
				n[20] ++;
				break;
			case 21:
				n[21] ++;
				break;
			case 22:
				n[22] ++;
				break;
			case 23:
				n[23] ++;
				break;
			}
		}
		System.out.println(n[0]+ "-"+ n[1]);
		for(int i = 0; i < n.length; i++)
		{
			n[i] = n[i] / numTotalInfrac;
			if(n[i] < 0.005) XS[i] = "";
			else if(n[i] < 0.01) XS[i] = "X";
			else if(n[i] < 0.015) XS[i] = "XX";
			else if(n[i] < 0.02) XS[i] = "XXX";
			else if(n[i] < 0.025) XS[i] = "XXXX";
			else if(n[i] < 0.03) XS[i] = "XXXXX";
			else if(n[i] < 0.035) XS[i] = "XXXXXX";
			else if(n[i] < 0.04) XS[i] = "XXXXXXX";
			else if(n[i] < 0.045) XS[i] = "XXXXXXXX";
			else if(n[i] < 0.05) XS[i] = "XXXXXXXXX";
			else if(n[i] < 0.055) XS[i] = "XXXXXXXXXX";
			else if(n[i] < 0.06) XS[i] = "XXXXXXXXXXX";
			else if(n[i] < 0.065) XS[i] = "XXXXXXXXXXXX";
			else if(n[i] < 0.07) XS[i] = "XXXXXXXXXXXXX";
			else if(n[i] < 0.075) XS[i] = "XXXXXXXXXXXXXX";
			else if(n[i] < 0.08) XS[i] = "XXXXXXXXXXXXXXX";
			else if(n[i] < 0.085) XS[i] = "XXXXXXXXXXXXXXXX";
			else if(n[i] < 0.09) XS[i] = "XXXXXXXXXXXXXXXXX";
			else if(n[i] < 0.095) XS[i] = "XXXXXXXXXXXXXXXXXX";
			else if(n[i] < 0.1) XS[i] = "XXXXXXXXXXXXXXXXXXX";
		}
		return XS;		
	}
	
	public String[] datosGrafica2()
	{
		String[] XS = new String[4];
		
		double[] FineAMT = new double[12];
		double[] penalty1 = new double[12];
		double[] penalty2 = new double[12];
		double[] totalPaid = new double[12];
		
		int indexMesInicial=0;
		
		VOMovingViolations[] lista = getArregloInfraccionesGeneral();
		Sort.ordenarQuickSort(lista,"ticketIssueDate");
		for(int i = 0; i < lista.length; i++){
			int mes = lista[i].getTicketIssueDate().getMonthValue();
			switch(mes)
			{
			case 1:
				indexMesInicial=0;
				FineAMT[0] = lista[i].getFineAMT().equals("")? FineAMT[0] + 0: FineAMT[0] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[0] = lista[i].getPenalty1().equals("")? penalty1[0]+ 0: penalty1[0]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[0] = lista[i].getPenalty2().equals("")? penalty2[0]+ 0: penalty2[0]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[0] = totalPaid[0]+ lista[i].getTotalPaid();
				break;
			case 2:
				indexMesInicial=0;
				FineAMT[1] = lista[i].getFineAMT().equals("")? FineAMT[1] + 0: FineAMT[1] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[1] = lista[i].getPenalty1().equals("")? penalty1[1]+ 0: penalty1[1]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[1] = lista[i].getPenalty2().equals("")? penalty2[1]+ 0: penalty2[1]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[1] = totalPaid[1]+ lista[i].getTotalPaid();
				break;
			case 3:
				indexMesInicial=0;
				FineAMT[2] = lista[i].getFineAMT().equals("")? FineAMT[2] + 0: FineAMT[2] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[2] = lista[i].getPenalty1().equals("")? penalty1[2]+ 0: penalty1[2]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[2] = lista[i].getPenalty2().equals("")? penalty2[2]+ 0: penalty2[2]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[2] = totalPaid[2]+ lista[i].getTotalPaid();
				break;
			case 4:
				indexMesInicial=0;
				FineAMT[3] = lista[i].getFineAMT().equals("")? FineAMT[3] + 0: FineAMT[3] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[3] = lista[i].getPenalty1().equals("")? penalty1[3]+ 0: penalty1[3]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[3] = lista[i].getPenalty2().equals("")? penalty2[3]+ 0: penalty2[3]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[3] = totalPaid[3]+ lista[i].getTotalPaid();
				break;
			case 5:
				indexMesInicial=4;
				FineAMT[4] = lista[i].getFineAMT().equals("")? FineAMT[4] + 0: FineAMT[4] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[4] = lista[i].getPenalty1().equals("")? penalty1[4]+ 0: penalty1[4]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[4] = lista[i].getPenalty2().equals("")? penalty2[4]+ 0: penalty2[4]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[4] = totalPaid[4]+ lista[i].getTotalPaid();
				break;
			case 6:
				indexMesInicial=4;
				FineAMT[5] = lista[i].getFineAMT().equals("")? FineAMT[5] + 0: FineAMT[5] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[5] = lista[i].getPenalty1().equals("")? penalty1[5]+ 0: penalty1[5]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[5] = lista[i].getPenalty2().equals("")? penalty2[5]+ 0: penalty2[5]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[5] = totalPaid[5]+ lista[i].getTotalPaid();
				break;
			case 7:
				indexMesInicial=4;
				FineAMT[6] = lista[i].getFineAMT().equals("")? FineAMT[6] + 0: FineAMT[6] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[6] = lista[i].getPenalty1().equals("")? penalty1[6]+ 0: penalty1[6]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[6] = lista[i].getPenalty2().equals("")? penalty2[6]+ 0: penalty2[6]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[6] = totalPaid[6]+ lista[i].getTotalPaid();
				break;
			case 8:
				indexMesInicial=4;
				FineAMT[7] = lista[i].getFineAMT().equals("")? FineAMT[7] + 0: FineAMT[7] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[7] = lista[i].getPenalty1().equals("")? penalty1[7]+ 0: penalty1[7]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[7] = lista[i].getPenalty2().equals("")? penalty2[7]+ 0: penalty2[7]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[7] = totalPaid[7]+ lista[i].getTotalPaid();
				break;
			case 9:
				indexMesInicial=8;
				FineAMT[8] = lista[i].getFineAMT().equals("")? FineAMT[8] + 0: FineAMT[8] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[8] = lista[i].getPenalty1().equals("")? penalty1[8]+ 0: penalty1[8]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[8] = lista[i].getPenalty2().equals("")? penalty2[8]+ 0: penalty2[8]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[8] = totalPaid[8]+ lista[i].getTotalPaid();
				break;
			case 10:
				indexMesInicial=8;
				FineAMT[9] = lista[i].getFineAMT().equals("")? FineAMT[9] + 0: FineAMT[9] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[9] = lista[i].getPenalty1().equals("")? penalty1[9]+ 0: penalty1[9]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[9] = lista[i].getPenalty2().equals("")? penalty2[9]+ 0: penalty2[9]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[9] = totalPaid[9]+ lista[i].getTotalPaid();
				break;
			case 11:
				indexMesInicial=8;
				FineAMT[10] = lista[i].getFineAMT().equals("")? FineAMT[10] + 0: FineAMT[10] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[10] = lista[i].getPenalty1().equals("")? penalty1[10]+ 0: penalty1[10]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[10] = lista[i].getPenalty2().equals("")? penalty2[10]+ 0: penalty2[10]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[10] = totalPaid[10]+ lista[i].getTotalPaid();
				break;
			case 12:
				indexMesInicial=8;
				FineAMT[11] = lista[i].getFineAMT().equals("")? FineAMT[11] + 0: FineAMT[11] + Integer.parseInt(lista[i].getFineAMT());
				penalty1[11] = lista[i].getPenalty1().equals("")? penalty1[11]+ 0: penalty1[11]+ Integer.parseInt(lista[i].getPenalty1());
				penalty2[11] = lista[i].getPenalty2().equals("")? penalty2[11]+ 0: penalty2[11]+ Integer.parseInt(lista[i].getPenalty2());
				totalPaid[11] = totalPaid[11]+ lista[i].getTotalPaid();
				break;
			}
			
		}
		double total1 = 0;
		double total2 = 0;
		double total3 = 0;
		double total4 = 0;
		
		for(int i = indexMesInicial; i< indexMesInicial+4; i++)
		{
			if(i==indexMesInicial)
			{
				total1 = FineAMT[i] + penalty1[i] + penalty2[i] - totalPaid[i];
				if(total1 < 1000000) XS[0] = "";
				else if(total1 < 2000000) XS[0] = "X";
				else if(total1 < 3000000) XS[0] = "XX";
				else if(total1 < 4000000) XS[0] = "XXX";
				else if(total1 < 5000000) XS[0] = "XXXX";
				else if(total1 < 6000000) XS[0] = "XXXXX";
				else if(total1 < 7000000) XS[0] = "XXXXXX";
				else if(total1 < 8000000) XS[0] = "XXXXXXX";
				else if(total1 < 9000000) XS[0] = "XXXXXXXX";
				else if(total1 < 10000000) XS[0] = "XXXXXXXXX";
				else if(total1 < 11000000) XS[0] = "XXXXXXXXXX";
				else if(total1 < 12000000) XS[0] = "XXXXXXXXXXX";
				else if(total1 < 13000000) XS[0] = "XXXXXXXXXXXX";
				else if(total1<  14000000) XS[0] = "XXXXXXXXXXXXX";
				else if(total1 < 15000000) XS[0] = "XXXXXXXXXXXXXX";
				else if(total1 < 16000000) XS[0] = "XXXXXXXXXXXXXXX";
				else if(total1 < 17000000) XS[0] = "XXXXXXXXXXXXXXXX";
				else if(total1 < 18000000) XS[0] = "XXXXXXXXXXXXXXXXX";
				else if(total1 < 19000000) XS[0] = "XXXXXXXXXXXXXXXXXX";
				else if(total1 < 20000000) XS[0] = "XXXXXXXXXXXXXXXXXXX";
			}
			if(i==indexMesInicial+1){
				total2 = total1 + FineAMT[i] + penalty1[i] + penalty2[i] - totalPaid[i];
				if(total2 < 1000000) XS[1] = "";
				else if(total2 < 2000000) XS[1] = "X";
				else if(total2 < 3000000) XS[1] = "XX";
				else if(total2 < 4000000) XS[1] = "XXX";
				else if(total2 < 5000000) XS[1] = "XXXX";
				else if(total2 < 6000000) XS[1] = "XXXXX";
				else if(total2 < 7000000) XS[1] = "XXXXXX";
				else if(total2 < 8000000) XS[1] = "XXXXXXX";
				else if(total2 < 9000000) XS[1] = "XXXXXXXX";
				else if(total2 < 10000000) XS[1] = "XXXXXXXXX";
				else if(total2 < 11000000) XS[1] = "XXXXXXXXXX";
				else if(total2 < 12000000) XS[1] = "XXXXXXXXXXX";
				else if(total2 < 13000000) XS[1] = "XXXXXXXXXXXX";
				else if(total2<  14000000) XS[1] = "XXXXXXXXXXXXX";
				else if(total2 < 15000000) XS[1] = "XXXXXXXXXXXXXX";
				else if(total2 < 16000000) XS[1] = "XXXXXXXXXXXXXXX";
				else if(total2 < 17000000) XS[1] = "XXXXXXXXXXXXXXXX";
				else if(total2 < 18000000) XS[1] = "XXXXXXXXXXXXXXXXX";
				else if(total2 < 19000000) XS[1] = "XXXXXXXXXXXXXXXXXX";
				else if(total2 < 20000000) XS[1] = "XXXXXXXXXXXXXXXXXXX";
			}
			if(i==indexMesInicial+2){
				total3 = total2 + FineAMT[i] + penalty1[i] + penalty2[i] - totalPaid[i];
				if(total3 < 1000000) XS[2] = "";
				else if(total3 < 2000000) XS[2] = "X";
				else if(total3 < 3000000) XS[2] = "XX";
				else if(total3 < 4000000) XS[2] = "XXX";
				else if(total3 < 5000000) XS[2] = "XXXX";
				else if(total3 < 6000000) XS[2] = "XXXXX";
				else if(total3 < 7000000) XS[2] = "XXXXXX";
				else if(total3 < 8000000) XS[2] = "XXXXXXX";
				else if(total3 < 9000000) XS[2] = "XXXXXXXX";
				else if(total3 < 10000000) XS[2] = "XXXXXXXXX";
				else if(total3 < 11000000) XS[2] = "XXXXXXXXXX";
				else if(total3 < 12000000) XS[2] = "XXXXXXXXXXX";
				else if(total3 < 13000000) XS[2] = "XXXXXXXXXXXX";
				else if(total3<  14000000) XS[2] = "XXXXXXXXXXXXX";
				else if(total3 < 15000000) XS[2] = "XXXXXXXXXXXXXX";
				else if(total3 < 16000000) XS[2] = "XXXXXXXXXXXXXXX";
				else if(total3 < 17000000) XS[2] = "XXXXXXXXXXXXXXXX";
				else if(total3 < 18000000) XS[2] = "XXXXXXXXXXXXXXXXX";
				else if(total3 < 19000000) XS[2] = "XXXXXXXXXXXXXXXXXX";
				else if(total3 < 20000000) XS[2] = "XXXXXXXXXXXXXXXXXXX";
			}
			if(i==indexMesInicial+3){
				total4 = total3 + FineAMT[i] + penalty1[i] + penalty2[i] - totalPaid[i];
				if(total4 < 1000000) XS[3] = "";
				else if(total4 < 2000000) XS[3] = "X";
				else if(total4 < 3000000) XS[3] = "XX";
				else if(total4 < 4000000) XS[3] = "XXX";
				else if(total4 < 5000000) XS[3] = "XXXX";
				else if(total4 < 6000000) XS[3] = "XXXXX";
				else if(total4 < 7000000) XS[3] = "XXXXXX";
				else if(total4 < 8000000) XS[3] = "XXXXXXX";
				else if(total4 < 9000000) XS[3] = "XXXXXXXX";
				else if(total4 < 10000000) XS[3] = "XXXXXXXXX";
				else if(total4 < 11000000) XS[3] = "XXXXXXXXXX";
				else if(total4 < 12000000) XS[3] = "XXXXXXXXXXX";
				else if(total4 < 13000000) XS[3] = "XXXXXXXXXXXX";
				else if(total4<  14000000) XS[3] = "XXXXXXXXXXXXX";
				else if(total4 < 15000000) XS[3] = "XXXXXXXXXXXXXX";
				else if(total4 < 16000000) XS[3] = "XXXXXXXXXXXXXXX";
				else if(total4 < 17000000) XS[3] = "XXXXXXXXXXXXXXXX";
				else if(total4 < 18000000) XS[3] = "XXXXXXXXXXXXXXXXX";
				else if(total4 < 19000000) XS[3] = "XXXXXXXXXXXXXXXXXX";
				else if(total4 < 20000000) XS[3] = "XXXXXXXXXXXXXXXXXXX";
				else if(total4 < 30000000) XS[3] = "XXXXXXXXXXXXXXXXXXXX";
				else if(total4 < 40000000) XS[3] = "XXXXXXXXXXXXXXXXXXXXX";
				else if(total4 < 50000000) XS[3] = "XXXXXXXXXXXXXXXXXXXXXX";
				else if(total4 < 60000000) XS[3] = "XXXXXXXXXXXXXXXXXXXXXXX";
				else if(total4 < 70000000) XS[3] = "XXXXXXXXXXXXXXXXXXXXXXXX";
				else if(total4 < 80000000) XS[3] = "XXXXXXXXXXXXXXXXXXXXXXXXX";
				else if(total4 < 90000000) XS[3] = "XXXXXXXXXXXXXXXXXXXXXXXXXX";
				else if(total4 < 100000000) XS[3] = "XXXXXXXXXXXXXXXXXXXXXXXXXX";
			}
			
		}
		return XS;
	}

	/**
	 * Retorna los elementos de la colaCuatrimestreInfracciones en un arreglo.
	 * @return
	 */
	public VOMovingViolations[] getArregloInfraccionesGeneral()
	{
		IteratorProyecto<VOMovingViolations> it=colaCuatrimestreInfracciones.iterator();
		VOMovingViolations[] infracciones=new VOMovingViolations[numTotalInfrac];
		int i=0;
		while(it.hasNext())
		{
			VOMovingViolations x=it.next();
			infracciones[i]=x;
			i++;
		}

		return infracciones;
	}
	/**
	 * Retorna los elementos de la cola en un arreglo.
	 * @return
	 */
	public VOMovingViolations[] getArregloInfraccionesPorCodigo(String codigoViolacion)
	{
		VOMovingViolations[] rta=new VOMovingViolations[numTotalInfrac];
		VOMovingViolations[] infracciones=getArregloInfraccionesGeneral();

		//Ordenar por ViolationCode
		Sort.ordenarQuickSort(infracciones,"violationCode");

		int i=0;

		for (VOMovingViolations voMovingViolations : infracciones) 
		{
			if(voMovingViolations.getViolationCode().equals(codigoViolacion))
			{
				rta[i]=voMovingViolations;	
			}
			i++;
		}

		return rta;
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha);
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
	/**
	 * Retorna un arreglo de infracciones A partir de una pila
	 */
	public VOMovingViolations[] getArregloInfraccionesPila(IStack<VOMovingViolations> s)
	{
		IteratorProyecto<VOMovingViolations> it= (IteratorProyecto<VOMovingViolations>) s.iterator();
		VOMovingViolations[] infracciones=new VOMovingViolations[s.size()];
		int i=0;
		while(it.hasNext())
		{
			VOMovingViolations x=it.next();
			infracciones[i]=x;
			i++;
		}

		return infracciones;
	}
	/**
	 * Retorna un arreglo de infracciones A partir de una cola
	 */
	public VOMovingViolations[] getArregloInfraccionesCola(IQueue<VOMovingViolations> s)
	{
		IteratorProyecto<VOMovingViolations> it= (IteratorProyecto<VOMovingViolations>) s.iterator();
		VOMovingViolations[] infracciones=new VOMovingViolations[s.size()];
		int i=0;
		while(it.hasNext())
		{
			VOMovingViolations x=it.next();
			infracciones[i]=x;
			i++;
		}

		return infracciones;
	}
}
