package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.IteratorProyecto;
import model.vo.VOMovingViolations;
import model.vo.VOViolationCode;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("0. Cargar datos del cuatrimestre");
		System.out.println("1. Verificar que OBJECTID es en realidad un identificador unico");
		System.out.println("2. Consultar infracciones por fecha/hora inicial y fecha/hora final");
		System.out.println("3. Dar FINEAMT promedio con y sin accidente por VIOLATIONCODE");
		System.out.println("4. Consultar infracciones por direccion entre fecha inicial y fecha final");

		
		System.out.println("5. Consultar los tipos de infracciones (VIOLATIONCODE) con su valor (FINEAMT) promedio en un rango dado");
		System.out.println("6. Consultar infracciones donde la cantidad pagada (TOTALPAID) esta en un rango dado. Se ordena por fecha de infraccioÌ�n");
		System.out.println("7. Consultar infracciones por hora inicial y hora final, ordenada ascendentemente por VIOLATIONDESC");
		System.out.println("8. Dado un tipo de infraccion (VIOLATIONCODE) informar el (FINEAMT) promedio y su desviacion estandar.");

		System.out.println("9. El numero de infracciones que ocurrieron en un rango de horas del dia. Se define el rango de horas por valores enteros en el rango [0, 24]");
		System.out.println("10. Grafica ASCII con el porcentaje de infracciones que tuvieron accidentes por hora del dia");
		System.out.println("11. La deuda (TOTALPAID - FINEAMT - PENALTY1 - PENALTY2) total por infracciones que se dieron en un rango de fechas.");
		System.out.println("12. Grafica ASCII con la deuda acumulada total por infracciones");

		
		System.out.println("13. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}
	
	public void printMovingViolationsReq2(IQueue<VOMovingViolations> resultados2) 
	{
		Iterator<VOMovingViolations> it=resultados2.iterator();
		int cont=0;
		while (it.hasNext()&&cont<20) 
		{
			VOMovingViolations v = (VOMovingViolations) it.next();
			System.out.println("ObjectID: " + v.objectId() + ", issued: " + v.getTicketIssueDate());
			cont++;
		}
	}
	
	public void printMovingViolationsReq4(IStack<VOMovingViolations> resultados4) 
	{
		System.out.println("OBJECTID\t TICKETISSUEDATE\t STREETSEGID\t ADDRESS_ID");

		Iterator<VOMovingViolations> it=resultados4.iterator();
		int cont=0;
		while (it.hasNext()&&cont<20) 
		{
			VOMovingViolations v = (VOMovingViolations) it.next();
			System.out.println(v.objectId() + "\t" + v.getTicketIssueDate() + "\t"+ v.getStreetSegId() + "\t"+ v.getAddressId());
			cont++;
		}
		
	}
	
	public void printViolationCodesReq5(IQueue<VOViolationCode> violationCodes) 
	{
		System.out.println("VIOLATIONCODE\t      FINEAMT promedio");

		Iterator<VOViolationCode> it=violationCodes.iterator();
		int cont=0;
		while (it.hasNext()&&cont<20) 
		{
			VOViolationCode v = (VOViolationCode) it.next();
			System.out.println("VIOLATIONCODE: "+v.getViolationCode() + "\t" +"FINEAMT: "+ v.getAvgFineAmt());
			cont++;
		}
			
	}
	
	public void printMovingViolationReq6(IStack<VOMovingViolations> resultados6) 
	{
		System.out.println("OBJECTID\t TICKETISSUEDATE\t TOTALPAID");
		Iterator<VOMovingViolations> it=resultados6.iterator();
		int cont=0;
		while (it.hasNext()&&cont<20) 
		{
			VOMovingViolations v = (VOMovingViolations) it.next();
			System.out.println( v.objectId() + "\t" + v.getTicketIssueDate() + "\t" + v.getTotalPaid());
			cont++;
		}
		
	}
	
	public void printMovingViolationsReq7(IQueue<VOMovingViolations> resultados7) 
	{
		System.out.println("OBJECTID\t TICKETISSUEDAT\t     VIOLATIONDESC");
		Iterator<VOMovingViolations> it=resultados7.iterator();
		int cont=0;
		while (it.hasNext()&&cont<20) 
		{
			VOMovingViolations v = (VOMovingViolations) it.next();
			System.out.println( v.objectId() + "\t" + v.getTicketIssueDate() + "\t" + v.getViolationDescription());
			cont++;
		}
	}
	
	public void printMovingViolationsByHourReq10(String[] x) 
	{
		System.out.println("Porcentaje de infracciones que tuvieron accidentes por hora. 2018");
		System.out.println("Hora| % de accidentes");
		System.out.println("00 | " + x[0]);
		System.out.println("01 | " + x[1]);
		System.out.println("02 | " + x[2]);
		System.out.println("03 | " + x[3]);
		System.out.println("04 | " + x[4]);
		System.out.println("05 | " + x[5]);
		System.out.println("06 | " + x[6]);
		System.out.println("07 | " + x[7]);
		System.out.println("08 | " + x[8]);
		System.out.println("09 | " + x[9]);
		System.out.println("10 | " + x[10]);
		System.out.println("11 | " + x[11]);
		System.out.println("12 | " + x[12]);
		System.out.println("13 | " + x[13]);
		System.out.println("14 | " + x[14]);
		System.out.println("15 | " + x[15]);
		System.out.println("16 | " + x[16]);
		System.out.println("17 | " + x[17]);
		System.out.println("18 | " + x[18]);
		System.out.println("19 | " + x[19]);
		System.out.println("20 | " + x[20]);
		System.out.println("21 | " + x[21]);
		System.out.println("22 | " + x[22]);
		System.out.println("23 | " + x[23]);
		System.out.println("");
		System.out.println("Cada X representa 0.5%");
	}
	
	public void printTotalDebtbyMonthReq12(String [] x)
	{
		System.out.println("Deuda acumulada por mes de infracciones. 2018");
		System.out.println("Mes| Dinero");
		System.out.println("01 | " + x[0]);
		System.out.println("02 | " + x[1]);
		System.out.println("03 | " + x[2]);
		System.out.println("04 | " + x[3]);
		System.out.println("");
		System.out.println("Cada X representa $1000000 USD");
	}
	
}
