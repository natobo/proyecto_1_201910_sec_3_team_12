package model.data_structures;

import java.util.Iterator;


import junit.framework.TestCase;

public class TestStack extends TestCase{
	/**
	 * Pila de strings donde se har�n las pruebas.
	 */
	private Stack<String>  pilaStrings;
	/**
	 * Pila de Integers donde se har�n las pruebas
	 */
	private Stack<Integer> pilaIntegers;
	
	public void setUp() 
	{
		try
		{
			pilaIntegers = new Stack<Integer>();
			//A�ade elementos a la cola de enteros.
			pilaIntegers.push(1);
			pilaIntegers.push(2);
			pilaIntegers.push(3);
			pilaIntegers.push(4);
			pilaIntegers.push(5);
			pilaIntegers.push(6);

			//Crea la cola de strings
			pilaStrings = new Stack<String>();
			//A�ade elementos a la cola de strings.
			pilaStrings.push("a");
			pilaStrings.push("b");
			pilaStrings.push("c");
			pilaStrings.push("d");
			pilaStrings.push("e");
			pilaStrings.push("f");
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			fail("Las pilas no se han podido inicializar");
		}
	}
	
	/**
	 * Comprueba si retorna el iterador de objetos de las pilas y si retorna el objeto con su tipo correspondiente
	 */
	public void testIterator() 
	{
		setUp();

		Iterator<Integer> itInteger = pilaIntegers.iterator();
		Iterator<String> itStrings = pilaStrings.iterator();

		assertNotNull(itInteger);
		assertNotNull(itStrings);

		int x=itInteger.next();

		System.out.println(x);
		assertEquals("Deberia ser un 6 el objeto retornado ", 6, x);

		String y=itStrings.next();

		assertEquals("Deberia ser una <f> el objeto retornado ", "f", y);
	}
	
	/**
	 * Comprueba si retorna correctamente el tama�o de cada pila.
	 */
	public void testSize()
	{
		setUp();

		assertTrue("No esta retornando el tama�o adecuado", pilaIntegers.size()==6);
		assertTrue("No esta retornando el tama�o adecuado", pilaStrings.size()==6);

		//Elimina un elemento para comprobar si cambia el tama�o
		pilaStrings.pop();
		pilaIntegers.pop();

		assertTrue("No esta retornando el tama�o adecuado", pilaIntegers.size()==5);
		assertTrue("No esta retornando el tama�o adecuado", pilaStrings.size()==5);
	}
	
	/**
	 * Comprueba si retorna correctamente el booleano.
	 */
	public void testIsEmpty()
	{
		setUp();

		assertFalse("Deberia ser falso", pilaIntegers.isEmpty());
		assertFalse("Deberia ser falso", pilaStrings.isEmpty());

		pilaIntegers.pop();
		pilaIntegers.pop();
		pilaIntegers.pop();
		pilaIntegers.pop();
		pilaIntegers.pop();
		pilaIntegers.pop();

		pilaStrings.pop();
		pilaStrings.pop();
		pilaStrings.pop();
		pilaStrings.pop();
		pilaStrings.pop();
		pilaStrings.pop();

		assertTrue("Deberia ser verdadero ", pilaIntegers.isEmpty());
		assertTrue("Deberia ser verdadero", pilaStrings.isEmpty());
	}
	
	/**
	 * Comprueba que si agrega un elemento 
	 */
	public void testPush()
	{
		setUp();

		pilaIntegers.push(9);

		Iterator<Integer> itInt = pilaIntegers.iterator();
		boolean loEncontro = false;
		while(itInt.hasNext()){
			int x = itInt.next();
			if(x == 9)
				loEncontro = true;
		}
		assertTrue("Deberia haber encontrado el elemento", loEncontro);

		pilaStrings.push("z");

		Iterator<String> itStr = pilaStrings.iterator();
		boolean loEncontro2 = false;
		while(itStr.hasNext()){
			String y=itStr.next();
			if(y.equalsIgnoreCase("z"))
				loEncontro2 = true;
		}
		assertTrue("Deberia haber encontrado el elemento", loEncontro2);
	}
	
	/**
	 * Comprueba si retorna el elemento top y si lo elimina de la pila
	 */
	public void testStack()
	{
		setUp();

		int x = pilaIntegers.pop();

		assertEquals("Deberia haber retornado un 6", 6, x);

		Iterator<Integer> itInt = pilaIntegers.iterator();
		boolean loEncontro = false;
		while(itInt.hasNext()){
			int y = itInt.next();
			if(y == 9)
				loEncontro=true;
		}

		assertFalse("No deberia haberlo encontrado ", loEncontro);

		String z = pilaStrings.pop();

		assertEquals("Deberia haber retornado una <f>","f",z);

		Iterator<String> itStr = pilaStrings.iterator();
		boolean loEncontro2 = false;
		while(itStr.hasNext()){
			String y = itStr.next();
			if(y.equalsIgnoreCase("f"))
				loEncontro2 = true;
		}

		assertFalse("No deberia haberlo encontrado ", loEncontro2);

	}
}
